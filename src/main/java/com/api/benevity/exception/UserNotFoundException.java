package com.api.benevity.exception;



public class UserNotFoundException extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4303280022687441229L;
	

	public UserNotFoundException(String roleName)
	{
		super("User"+ roleName+" not found");
	}


	public UserNotFoundException() {
		// TODO Auto-generated constructor stub
		super("Users not found");
	}

}
