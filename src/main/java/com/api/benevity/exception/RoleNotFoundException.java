package com.api.benevity.exception;



public class RoleNotFoundException extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4553227492266264260L;

	public RoleNotFoundException(String roleName)
	{
		super("Role"+ roleName+" not found");
	}

	public RoleNotFoundException() {
		// TODO Auto-generated constructor stub
		
		super("Role not found");
	}

}
