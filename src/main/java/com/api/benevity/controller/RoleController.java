package com.api.benevity.controller;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.benevity.exception.RoleNotFoundException;
import com.api.benevity.model.Role;
import com.api.benevity.model.User;
import com.api.benevity.service.RoleService;
import com.api.benevity.service.UserService;


@RestController
@RequestMapping("/api/v1/role")
public class RoleController {
	@Autowired
	RoleService roleService ; 
	@Autowired
	UserService userService;
	
	@GetMapping
	public ResponseEntity<List<Role>> getAllRoles() {
		
		List<Role> roleList =  roleService.getAllRoles();
		return new ResponseEntity<>(roleList, HttpStatus.OK);
	
		}
	@GetMapping({"/{roleId}"})
	public ResponseEntity<Role> getRoleById(@PathVariable int roleId) {
		
		Role role =  roleService.getRoleById(roleId);
		return new ResponseEntity<>(role, HttpStatus.OK);
	
		}
	
	
	@PostMapping({"/{roleId}/members"})
	public ResponseEntity<Role> addUsersToRole(@PathVariable int roleId,@RequestBody Set<User> users){
		
		try { 
			Role role = roleService.getRoleById(roleId);
		  if(role==null)throw new RoleNotFoundException();
		  if(role.getUsers() == null) {
			  role.setUsers(users);
		  }else {
		    role.getUsers().addAll(users);
		  }
		    roleService.insertRole(role);
		return new ResponseEntity<>(role, HttpStatus.OK);
		}catch(RoleNotFoundException e) { return
				 ResponseEntity.status(HttpStatus.NOT_FOUND).build(); }
	}
	
	@DeleteMapping({"/{roleId}/members"})
    public ResponseEntity<Role> removeUsersFromRole(@PathVariable int roleId,@RequestBody Set<User> users){
		
		try { 
			Role role = roleService.getRoleById(roleId);
		  if(role==null)throw new RoleNotFoundException();
		    
		    role.getUsers().retainAll(users);
		    roleService.insertRole(role);
		return new ResponseEntity<>(role, HttpStatus.OK);
		}catch(RoleNotFoundException e) { return
				 ResponseEntity.status(HttpStatus.NOT_FOUND).build(); }
	}	
	@PostMapping
	public ResponseEntity<Role> createRole(@RequestBody Role role) {
	
		Role rolePersist = (Role) roleService.insertRole(role);
		HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("role", "/api/v1/role/" + rolePersist.getRoleName());
		return new ResponseEntity<>(rolePersist, HttpStatus.OK);
	}
	@PutMapping({"/{roleId}"})
	public ResponseEntity<Role> updateRole (@PathVariable int roleId, @RequestBody Role role) {
		
		roleService.updateRole(roleId, role);
		return new ResponseEntity<>(roleService.getRoleById(roleId), HttpStatus.OK);
	}
	@DeleteMapping({"/{roleId}"})
	public ResponseEntity<Role> deleteRole (@PathVariable int roleId) {
		
		roleService.deleteRole(roleId);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	
}
