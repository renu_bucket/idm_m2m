package com.api.benevity.controller;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.api.benevity.exception.UserNotFoundException;
import com.api.benevity.model.Role;
import com.api.benevity.model.User;
import com.api.benevity.service.RoleService;
import com.api.benevity.service.UserService;


@RestController
@RequestMapping("/api/v1/user")
public class UserController {
	@Autowired
	UserService userService ; 
	@Autowired
	RoleService roleService;
	
	@GetMapping
	public ResponseEntity<List<User>> getAllUser(@RequestParam(name="familyName", required = false) String familyName) {
		try {
			
			List<User> userList=null;
		if (familyName ==null || "".equals(familyName))	
			userList =  userService.getAllUsers();
		else 
			userList =  userService.getUsersByFamilyName(familyName);
		if(userList==null)throw new UserNotFoundException();
		return new ResponseEntity<>(userList, HttpStatus.OK);
		}catch (UserNotFoundException e) {
	        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
	    }
		}
	@GetMapping({"/{userId}"})
	public ResponseEntity<User> getUserById(@PathVariable int userId) {
		try {
		User user =  userService.getUserById(userId);
		if(user==null)throw new UserNotFoundException(Integer.toString(userId));
		return new ResponseEntity<>(user, HttpStatus.OK);
		}catch ( UserNotFoundException e) {
	        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
	    }
		}
	
		
	@DeleteMapping({"/{userId}/memberof"})
	public ResponseEntity<User> removeRoles(@PathVariable int userId, @RequestBody Set<Role> roles){
		User user=null;
		try { 
			 user = userService.getUserById(userId);
		  if(user==null)throw new UserNotFoundException();
		    user.getRoles().removeAll(roles);
		    userService.insertUser(user);
		
		}catch( UserNotFoundException e) { return
				 ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<>(user, HttpStatus.CREATED);
		
	}
	@PostMapping({"/{userId}/memberof"})
	
	public ResponseEntity<User> addRoles(@PathVariable int userId, @RequestBody Set<Role> roles){
		User user=null;
		try { 
			 user = userService.getUserById(userId);
		  if(user==null)throw new UserNotFoundException();
		    //Set<Role> roles = roleService.getRolesByIds(roleIds);
		  if(user.getRoles()==null) {
		    user.setRoles(roles);
		    }else {
		    	user.getRoles().addAll(roles);
		    }
		    userService.insertUser(user);
		
		}catch( UserNotFoundException e) { return
				 ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<>(user, HttpStatus.CREATED);
		
	}
	@PostMapping
	public ResponseEntity<User> createUser(@RequestBody User user) {
		 
		User user1 = (User) userService.insertUser(user);
		HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("user", "/api/v1/user/" + user1.getUserID());
		return new ResponseEntity<>(user1, HttpStatus.CREATED);
		 
	}
	@PutMapping({"/{userId}"})
	public ResponseEntity<User> updateUser (@PathVariable int userId, @RequestBody User user) {
		try {
		userService.updateUser(userId, user);
		return new ResponseEntity<>(userService.getUserById(userId), HttpStatus.OK);
		}catch( UserNotFoundException e) { return
				 ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
	}
	@DeleteMapping({"/{userId}"})
	public ResponseEntity<User> deleteUser (@PathVariable int userId) {
		try {
		userService.deleteUser(userId);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}catch( UserNotFoundException e) { return
				 ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
	}
	
	

}
