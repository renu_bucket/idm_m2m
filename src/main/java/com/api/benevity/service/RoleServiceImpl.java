package com.api.benevity.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.api.benevity.model.Role;
import com.api.benevity.repositories.RoleRepository;

@Service

public class RoleServiceImpl implements RoleService {
	@Autowired
	RoleRepository roleRepository;

	/*
	 * @Autowired private UserService userService;
	 */
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(RoleServiceImpl.class);

	public RoleServiceImpl(RoleRepository roleRepository) {
		this.roleRepository = roleRepository;
	}

	@Override
	public List<Role> getAllRoles() {
		// TODO Auto-generated method stub

		logger.info("getAllRoles start.");
		List<Role> roles = new ArrayList<>();
		roleRepository.findAll().forEach(roles::add);
		roles.stream().forEach(role -> System.out.println(role.getRoleName()));
		return roles;
	}

	@Override
	public Role getRoleById(int id) {
		// TODO Auto-generated method stub
		logger.info("getRoleById start.");
		return roleRepository.findById(id).get();
	}

	@Override
	public Set<Role> getRolesByIds(List<Integer> ids) {
		// TODO Auto-generated method stub
		logger.info("getRolesByIds start.");
		Set<Role> roles = new HashSet<>();
		roleRepository.findAllById(ids).forEach(roles::add);
		return roles;

	}


	@Override
	public Role insertRole(Role role) {
		// TODO Auto-generated method stub
		logger.info("insertRole start.");
		return roleRepository.save(role);
	}

	@Override
	public void updateRole(int id, Role role) {
		// TODO Auto-generated method stub

		Role roleFromDB = roleRepository.findById(id).get();
		System.out.println(roleFromDB.toString());
		//roleFromDB.setRoleID(role.getRoleID());
		roleFromDB.setRoleName(role.getRoleName());
		roleFromDB.setRoleType(role.getRoleType());
		roleFromDB.setRoleDescription(role.getRoleDescription());
		roleFromDB.setUsers(role.getUsers());
		roleRepository.save(roleFromDB);

	}

	@Override
	public void deleteRole(int id) {
		// TODO Auto-generated method stub
		roleRepository.deleteById(id);

	}

}
