package com.api.benevity.service;

import java.util.List;
import java.util.Set;

import com.api.benevity.exception.UserNotFoundException;
import com.api.benevity.model.User;


public interface UserService {

	List<User> getAllUsers() throws UserNotFoundException;

	 User getUserById(int empno) throws UserNotFoundException;

	 User insertUser(User emp);

	    void updateUser(int empno, User emp) throws UserNotFoundException;

	    void deleteUser(int empno) throws UserNotFoundException;

		Set<User> getUsersByIds(List<Integer> ids);

		List<User> getUsersByFamilyName(String familyName);

		
}
