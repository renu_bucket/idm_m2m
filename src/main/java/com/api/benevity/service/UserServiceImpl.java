package com.api.benevity.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.api.benevity.exception.UserNotFoundException;
import com.api.benevity.model.User;
import com.api.benevity.repositories.UserRepository;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
	UserRepository userRepository;
	
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

	public UserServiceImpl(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	public List<User> getAllUsers() throws UserNotFoundException {
		// TODO Auto-generated method stub
		logger.info("getAllUsers start.");
		try {
		List<User> userList = new ArrayList<>();
		userRepository.findAll().forEach(userList::add);
		return userList;
		}catch( NoSuchElementException |EmptyResultDataAccessException e ) { 
			throw new UserNotFoundException();
		}
	}

	@Override
	public Set<User> getUsersByIds(List<Integer> ids) {
		// TODO Auto-generated method stub
		logger.info("getUsersByIds start.");
		Set<User> users = new HashSet<>();
		userRepository.findAllById(ids).forEach(users::add);
		return users;
	}

	@Override
	public User getUserById(int userno) throws UserNotFoundException {
		// TODO Auto-generated method stub
		logger.info("getUserById start.");
		try {
		return userRepository.findById(userno).get();
		}catch( NoSuchElementException |EmptyResultDataAccessException e ) { 
			throw new UserNotFoundException();
		}
	}

 
	@Override
	public User insertUser(User user) {
		// TODO Auto-generated method stub
		logger.info("insertUser start.");
		return userRepository.save(user);
	}

	@Override
	public void updateUser(int userID, User user) throws UserNotFoundException {
		// TODO Auto-generated method stub
		logger.info("updateUser start.");
		try {
		User userFromDB = userRepository.findById(userID).get();
		//userFromDB.setUserID(user.getUserID());
		userFromDB.setFamilyName(user.getFamilyName());
		userFromDB.setGivenName(user.getGivenName());
		userFromDB.setEmail(user.getEmail());
		userFromDB.setDateOfBirth(user.getDateOfBirth());
		userFromDB.setRoles(user.getRoles());
		userRepository.save(userFromDB);
		}catch( NoSuchElementException |EmptyResultDataAccessException e ) { 
			throw new UserNotFoundException();
		}
			
		}

	

	@Override
	public void deleteUser(int userID) throws UserNotFoundException{
		// TODO Auto-generated method stub
		logger.info("deleteUser start.");
		try {
		userRepository.deleteById(userID);
		}catch( NoSuchElementException |EmptyResultDataAccessException e ) { 
			throw new UserNotFoundException();
		}
	}

	@Override
	public List<User> getUsersByFamilyName(String familyName) {
		// TODO Auto-generated method stub
		return userRepository.findByFamilyNameContaining(familyName);
	}

}
