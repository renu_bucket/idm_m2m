package com.api.benevity.service;

import java.util.List;
import java.util.Set;

import com.api.benevity.model.Role;


public interface RoleService {
	 List<Role> getAllRoles();

	 Role getRoleById(int id);

	 Role insertRole(Role model);

	    void updateRole(int id, Role model);

	    void deleteRole(int id);

		Set<Role> getRolesByIds(List<Integer> ids);

	
}
