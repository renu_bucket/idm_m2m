package com.api.benevity.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(name="USER")
public class User implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5564506422054016038L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int userID;
	@Column
	private String familyName; 
	@Column
	private String givenName; 
	@Column
	private String email;
	@Column
	private String password; 
	@Column
	private Timestamp dateOfBirth; 
    @CreationTimestamp
    @Column(updatable = false)
    private Timestamp dateCreated;
    @UpdateTimestamp
    private Timestamp lastModified;
	
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "ENTITELMENT",
            joinColumns = {
                    @JoinColumn(name = "userID", referencedColumnName = "userID",
                            nullable = false, updatable = false)},
            inverseJoinColumns = {
                    @JoinColumn(name = "roleID", referencedColumnName = "roleID",
                            nullable = false, updatable = false)})
       private Set<Role> roles = new HashSet<>();
    
    	
	public User() {
		super();
	}
	public User(int userID, String familyName, String givenName, String email, String password, Timestamp dateOfBirth) {
		super();
		this.userID = userID;
		this.familyName = familyName;
		this.givenName = givenName;
		this.email = email;
		this.password = password;
		this.dateOfBirth = dateOfBirth;
	}
	public int getUserID() {
		return userID;
	}
	public Set<Role> getRoles() {
		return roles;
	}
	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}
	public void setUserID(int userID) {
		this.userID = userID;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFamilyName() {
		return familyName;
	}
	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}
	public String getGivenName() {
		return givenName;
	}
	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Timestamp getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Timestamp dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public Timestamp getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(Timestamp dateCreated) {
		this.dateCreated = dateCreated;
	}
	public Timestamp getLastModified() {
		return lastModified;
	}
	public void setLastModified(Timestamp lastModified) {
		this.lastModified = lastModified;
	}
	
	@Override
	public int hashCode() {
		return userID;
		
	}
	
	@Override
	public boolean equals(Object o) {
	
	// If the object is compared with itself then return true  
    if (o == this) {
        return true;
    }

    /* Check if o is an instance of User or not
      "null instanceof [type]" also returns false */
    if (!(o instanceof Role)) {
        return false;
    }
     User r= (User)o;
    if (this.getUserID()== r.getUserID()) {
    	return true;
	}else {
		return false;
	}
	}
		
	 @Override
	    public String toString() {
	        return "Model_Rest [userID=" + userID + ", givenName=" + givenName +", familyName=" + familyName + "]";
	    }
   

}
