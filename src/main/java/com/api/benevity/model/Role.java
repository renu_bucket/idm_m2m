package com.api.benevity.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="ROLE")
public class Role implements Serializable {
    
	/**
	 * 
	 */
	private static final long serialVersionUID = -1584869397402110057L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int roleID;
	@Column
	private String roleName;
	@Column
	private String roleDescription;
	private String roleType;
    @CreationTimestamp
    @Column(updatable = false)
    private Timestamp dateCreated;
    @UpdateTimestamp
    private Timestamp lastModified;
    
    @ManyToMany(mappedBy = "roles", fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<User> users = new HashSet<>();
	
    
	public Role() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	public Role(int roleID, String roleName, String roleDescription, String roleType) {
		super();
		this.roleID = roleID;
		this.roleName = roleName;
		this.roleDescription = roleDescription;
		this.roleType = roleType;
	}

	public Set<User> getUsers() {
		return users;
	}
	public void setUsers(Set<User> users) {
		this.users = users;
	}
	public int getRoleID() {
		return roleID;
	}
	public void setRoleID(int roleID) {
		this.roleID = roleID;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getRoleDescription() {
		return roleDescription;
	}
	public void setRoleDescription(String roleDescription) {
		this.roleDescription = roleDescription;
	}
	public Timestamp getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(Timestamp dateCreated) {
		this.dateCreated = dateCreated;
	}
	public Timestamp getLastModified() {
		return lastModified;
	}
	public void setLastModified(Timestamp lastModified) {
		this.lastModified = lastModified;
	}
	public String getRoleType() {
		return roleType;
	}
	public void setRoleType(String roleType) {
		this.roleType = roleType;
		this.hashCode();
	}
	
	@Override
	public int hashCode() {
		return roleID;
		
	}
	
	@Override
	public boolean equals(Object o) {
	
	// If the object is compared with itself then return true  
    if (o == this) {
        return true;
    }

    /* Check if o is an instance of Role or not
      "null instanceof [type]" also returns false */
    if (!(o instanceof Role)) {
        return false;
    }
     Role r= (Role)o;
    if (this.getRoleID()== r.getRoleID()) {
    	return true;
	}else {
		return false;
	}
	}
		
	 @Override
	    public String toString() {
	        return "Model_Rest [roleID=" + roleID + ", roleName=" + roleName + "]";
	    }
    
    
}
