package com.api.benevity.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ENTITELMENT")
public class Entitelment implements Serializable {
    
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 62467883478054764L;
	@Id
	private int roleID;
	@Id
	private int userID;
    @Column(updatable = false)
    private Timestamp dateGranted;
	public int getRoleID() {
		return roleID;
	}
	public void setRoleID(int roleID) {
		this.roleID = roleID;
	}
	public int getUserID() {
		return userID;
	}
	public void setUserID(int userID) {
		this.userID = userID;
	}
	public Timestamp getDateGranted() {
		return dateGranted;
	}
	public void setDateGranted(Timestamp dateGranted) {
		this.dateGranted = dateGranted;
	}
	
}
