package com.api.benevity.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.api.benevity.model.Role;
@Repository
public interface RoleRepository extends CrudRepository<Role, Integer> {

}
