package com.api.benevity.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


import com.api.benevity.model.User;

@Repository

public interface UserRepository extends CrudRepository<User, Integer> {

		  List<User> findByFamilyNameContaining(String familyName);
}
