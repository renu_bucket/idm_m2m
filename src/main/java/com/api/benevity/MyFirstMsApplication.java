package com.api.benevity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyFirstMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyFirstMsApplication.class, args);
	}

}
