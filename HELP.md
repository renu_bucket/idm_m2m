# Getting Started

## Application Design

The Spring Boot Application deals with Identity and Role management.

### Entities

   USER : 
   
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int userID;
	@Column
	private String familyName; 
	@Column
	private String givenName; 
	@Column
	private String email;
	@Column
	private String password; 
	@Column
	private Timestamp dateOfBirth; 
    @CreationTimestamp
    @Column(updatable = false)
    private Timestamp dateCreated;
    @UpdateTimestamp
    private Timestamp lastModified;
    
   ROLE :
   
     @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int roleID;
	@Column
	private String roleName;
	@Column
	private String roleDescription;
	private String roleType;
    @CreationTimestamp
    @Column(updatable = false)
    private Timestamp dateCreated;
    @UpdateTimestamp
    private Timestamp lastModified;
    
   ENTITELMENT :
     
    @Column
    private int userID;
    @Column
    private int roleID;
    @Column(updatable = false)
    private Timestamp dateGranted; //Get ToBe Implemented.


### ResourceTypes:

#### /api/v1/user: 


   * Allows CRUD(GET/POST/PUT/DELETE) functions to manage Identities.
   
   
      GET -->/api/v1/user  or /api/v1/user/<ID>  
     
      POST -->/api/v1/user
      
      PUT -->/api/v1/user/<ID>
      
      DELETE -->/api/v1/user/<ID>
     
     
   * Allows SubresourceType management on Roles(MemberOF), Password, Identity Status.
   
     
     
     Provision Role to user:
   
      POST-->/api/v1/user/<ID>/memberof -- roles PayLoad to be send
   
     De-Provision Role from user:
   
      DELETE-->/api/v1/user/<ID>/memberof--roles PayLoad to be send
   
     Manage Password:  
   
      PUT -->/api/v1/user/<ID>/password -- ToBe Implemented
   
     Manage Account
   
      PUT -->/api/v1/user/<ID>/account -- ToBe Implemented
   
   
   * search users by family name 
   
      GET -->/api/v1/user?familyName=<LastName>
      
#### /api/v1/role:

   * Allows CRUD(GET/POST/PUT/DELETE) functions to manage Roles.
   
      GET -->/api/v1/role/<ID>
      
      POST -->/api/v1/role
      
      PUT -->/api/v1/role/<ID>
      
      DELETE -->/api/v1/role/<ID>
      
      
   * Allows SubresourceType management on users (Members)
   
   
    POST -->/api/v1/role/<ID>/members -- users pay load to be send
    
    DELETE-->/api/v1/role/<ID>/members -- users pay load to be send
      
      
## UnitTest

   ** ToBe Implemented 
   
## PostManTest  

### User

* CreateUser

    Method : POST 

    URI: http://localhost:8080/api/v1/user/

    Content-Type: application/json

    payLoad:
    {
	     "familyName": "Renu2",
        "givenName": "Pathi02",
        "email": "er.pathi2@gmail.com",
        "password": "password4",
        "dateOfBirth": "2015-07-15T00:00:00.000+00:00"
    } 
    
    
 * GetAllUser
 
    Method: GET
 
    URI: http://localhost:8080/api/v1/user
 
    Content-Type: application/json
       
 * GetSpecificUser
 
    Method: GET
 
    URI: http://localhost:8080/api/v1/user/<id>
 
    Content-Type: application/json
 
 * GetAllUsers In One Family
 
    Method: GET
 
    URI: http://localhost:8080/api/v1/user?familyName=<familyName>
 
    Content-Type: application/json
 
 * DeleteUser
 
    Method: DELETE
 
    URI: http://localhost:8080/api/v1/user/<id>
 
    Content-Type: application/json
 
 * UpdateUser
 
    Method: PUT
 
    URI: http://localhost:8080/api/v1/user/<id>
 
    Content-Type: application/json
 
    PayLoad:
    {
	     "familyName": "Renu2",
        "givenName": "Pathi02",
        "email": "er.pathi2@gmail.com",
        "password": "password4",
        "dateOfBirth": "2015-07-15T00:00:00.000+00:00"
    } 
    
    
 ### UpdateUserRoles
 
   * Add Role:
 
      Method: POST
 
      URI: http://localhost:8080/api/v1/user/<id>/memberof
 
        Content-Type: application/json
  
      PayLoad:
      [{"roleID": <roleID>}{"roleID": <roleID>}]
 
   * Remove Role:   
 
      Method: DELETE
 
      URI: http://localhost:8080/api/v1/user/<id>/memberof
 
      Content-Type: application/json
 
      PayLoad:
      [{"roleID": <roleID>}{"roleID": <roleID>}]
  
  
### Role
  
  * CreateRole
  
     Method : POST 

     URI: http://localhost:8080/api/v1/role/

     Content-Type: application/json

     payLoad:
     {
      "roleName": "app1_function5",
      "roleDescription": "app1_function5",
      "roleType": "FunctionalRole"
     }
    
  * GetAllRoles
 
     Method: GET
 
     URI: http://localhost:8080/api/v1/role
 
     Content-Type: application/json
      
  * GetSpecificRole
 
     Method: GET
 
     URI: http://localhost:8080/api/v1/role/<id>
 
     Content-Type: application/json

  * DeleteUser
 
     Method: DELETE
 
     URI: http://localhost:8080/api/v1/role/<id>
 
     Content-Type: application/json
 
  * UpdateUser
 
     Method: PUT
 
     URI: http://localhost:8080/api/v1/role/<id>
 
     Content-Type: application/json
 
     PayLoad:
     {
      "roleName": "app1_function5",
      "roleDescription": "app1_function5",
      "roleType": "FunctionalRole"
     } 
    
 ### UpdateRoleMembers
 
  * Add User:
 
    Method: POST
 
    URI: http://localhost:8080/api/v1/role/<id>/members
 
    Content-Type: application/json
 
    PayLoad:
    [{"userID": <userID>}{"userID": <userID>}]
 
  * Remove Users:   
 
    Method: DELETE
 
    URI: http://localhost:8080/api/v1/role/<id>/members
 
    Content-Type: application/json
 
    PayLoad:
    [{"userID": <userID>}{"userID": <userID>}]
      
### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.5.2/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.5.2/maven-plugin/reference/html/#build-image)
* [Spring Data JPA](https://docs.spring.io/spring-boot/docs/2.5.2/reference/htmlsingle/#boot-features-jpa-and-spring-data)
* [OAuth2 Resource Server](https://docs.spring.io/spring-boot/docs/2.5.2/reference/htmlsingle/#boot-features-security-oauth2-server)
* [Okta Spring Boot documentation](https://github.com/okta/okta-spring-boot#readme)
* [OAuth2 Client](https://docs.spring.io/spring-boot/docs/2.5.2/reference/htmlsingle/#boot-features-security-oauth2-client)
* [Rest Repositories](https://docs.spring.io/spring-boot/docs/2.5.2/reference/htmlsingle/#howto-use-exposing-spring-data-repositories-rest-endpoint)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.5.2/reference/htmlsingle/#boot-features-developing-web-applications)

### Guides
The following guides illustrate how to use some features concretely:

* [Accessing Data with JPA](https://spring.io/guides/gs/accessing-data-jpa/)
* [Okta-Hosted Login Page Example](https://github.com/okta/samples-java-spring/tree/master/okta-hosted-login)
* [Custom Login Page Example](https://github.com/okta/samples-java-spring/tree/master/custom-login)
* [Okta Spring Security Resource Server Example](https://github.com/okta/samples-java-spring/tree/master/resource-server)
* [Accessing JPA Data with REST](https://spring.io/guides/gs/accessing-data-rest/)
* [Accessing Neo4j Data with REST](https://spring.io/guides/gs/accessing-neo4j-data-rest/)
* [Accessing MongoDB Data with REST](https://spring.io/guides/gs/accessing-mongodb-data-rest/)
* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/bookmarks/)

## OAuth 2.0 and OIDC with Okta

If you don't have a free Okta developer account, you can create one with [the Okta CLI](https://cli.okta.com):

```bash
$ okta register
```

Then, register your Spring Boot app on Okta using:

```bash
$ okta apps create
```

Select **Web** > **Okta Spring Boot Starter** and accept the default redirect URIs.

